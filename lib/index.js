"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Orientation = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Orientation =
/*#__PURE__*/
function (_Component) {
  _inherits(Orientation, _Component);

  function Orientation() {
    _classCallCheck(this, Orientation);

    return _possibleConstructorReturn(this, _getPrototypeOf(Orientation).apply(this, arguments));
  }

  _createClass(Orientation, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          orientation = _this$props.orientation,
          children = _this$props.children,
          className = _this$props.className;
      return _react.default.createElement("div", {
        className: "".concat(className, " react-orientation react-orientation--").concat(orientation)
      }, children);
    }
  }]);

  return Orientation;
}(_react.Component);

exports.Orientation = Orientation;
Orientation.propTypes = {
  alwaysRender: _propTypes.default.bool,
  children: _propTypes.default.node,
  className: _propTypes.default.string,
  orientation: _propTypes.default.oneOf(['portrait', 'landscape']).isRequired
};
Orientation.defaultProps = {
  className: '',
  alwaysRender: true
};

var noop = function noop() {
  return false;
};

window.screen.lockOrientationUniversal = window.screen.lockOrientation || window.screen.mozLockOrientation || window.screen.msLockOrientation;

var lock = function lock(orientation) {
  var _window = window,
      screen = _window.screen;

  if (screen.orientation && typeof screen.orientation.lock === 'function') {
    return window.screen.orientation.lock(orientation);
  } else if (screen.lockOrientationUniversal) {
    return new Promise(function (resolve, reject) {
      if (screen.lockOrientationUniversal(orientation)) {
        resolve();
      } else {
        reject();
      }
    });
  } else {
    return new Promise(function (resolve, reject) {
      return reject();
    });
  }
};

var DeviceOrientation =
/*#__PURE__*/
function (_Component2) {
  _inherits(DeviceOrientation, _Component2);

  function DeviceOrientation(props) {
    var _this;

    _classCallCheck(this, DeviceOrientation);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DeviceOrientation).call(this, props));

    _this.lockOrientation(props);

    _this.onOrientationChange = _this.onOrientationChange.bind(_assertThisInitialized(_this));
    _this.state = {
      orientation: null,
      type: null,
      angle: null
    };
    return _this;
  }

  _createClass(DeviceOrientation, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      this.onOrientationChange(null);
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      console.log('DeviceOrientation', 'componentDidMount');

      if (window.screen.orientation && 'onchange' in window.screen.orientation) {
        console.log('Using screen.orientation.onchange');
        window.screen.orientation.addEventListener('change', this.onOrientationChange);
      } else if ('onorientationchange' in window) {
        console.log('Using window.onorientationchange');
        window.addEventListener('orientationchange', this.onOrientationChange);
      } else {
        console.warn('No orientationchange events');
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      console.log('DeviceOrientation', 'componentWillUnmount');

      if (window.screen.orientation && 'onchange' in window.screen.orientation) {
        console.log('Removing screen.orientation.onchange');
        window.screen.orientation.removeEventListener('change', this.onOrientationChange);
      } else if ('onorientationchange' in window) {
        console.log('Removing window.onorientationchange');
        window.removeEventListener('orientationchange', this.onOrientationChange);
      }
    }
  }, {
    key: "onOrientationChange",
    value: function onOrientationChange(event) {
      var onOrientationChange = this.props.onOrientationChange || noop;
      var orientation = 'portrait';
      var type = 'primary';
      var angle = 0;

      if (window.orientation) {
        angle = window.orientation;
        orientation = Math.abs(angle) === 90 ? 'landscape' : 'portrait';
      }

      if (window.screen.orientation) {
        var _window$screen$orient = window.screen.orientation.type.split('-');

        var _window$screen$orient2 = _slicedToArray(_window$screen$orient, 2);

        orientation = _window$screen$orient2[0];
        type = _window$screen$orient2[1];
        angle = window.screen.orientation;
      }

      this.setState({
        orientation: orientation,
        type: type,
        angle: angle
      });
      onOrientationChange(orientation, type, angle);
    }
  }, {
    key: "lockOrientation",
    value: function lockOrientation(_ref) {
      var _lockOrientation = _ref.lockOrientation;

      if (typeof _lockOrientation !== 'string') {
        return;
      }

      var onLockOrientation = this.props.onLockOrientation || noop;
      return lock(_lockOrientation).then(function () {
        onLockOrientation(true);
      }).catch(function () {
        onLockOrientation(false);
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          children = _this$props2.children,
          className = _this$props2.className;
      var orientation = this.state.orientation;
      return _react.default.createElement("div", {
        className: "".concat(className)
      }, _react.Children.map(children, function (child) {
        var props = child.props;

        if (props.alwaysRender || props.orientation === orientation) {
          return child; // } else {
          //   console.log('Skipping child', child)
        }
      }));
    }
  }]);

  return DeviceOrientation;
}(_react.Component); // https://developer.mozilla.org/en-US/docs/Web/API/screen/lockOrientation


exports.default = DeviceOrientation;
var LOCK_ORIENTATIONS = ['portrait-primary', 'portrait-secondary', 'landscape-primary', 'landscape-secondary', 'portrait', 'landscape', 'default'];

var isOrientation = function isOrientation(props, propName, componentName, location, propFullName) {
  var propValue = props[propName];

  if (propValue.type !== Orientation) {
    return new Error("Invalid ".concat(location, " '").concat(propFullName, "' supplied to '").concat(componentName, "', expected 'Orientation' component."));
  }
};

DeviceOrientation.propTypes = {
  children: _propTypes.default.oneOfType([isOrientation, _propTypes.default.arrayOf(isOrientation)]).isRequired,
  className: _propTypes.default.string,
  lockOrientation: _propTypes.default.oneOfType([_propTypes.default.oneOf(LOCK_ORIENTATIONS), _propTypes.default.arrayOf(_propTypes.default.oneOf(LOCK_ORIENTATIONS))]),
  onLockOrientation: _propTypes.default.func,
  onOrientationChange: _propTypes.default.func
};
DeviceOrientation.defaultProps = {
  className: ''
};